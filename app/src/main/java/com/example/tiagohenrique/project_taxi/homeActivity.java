package com.example.tiagohenrique.project_taxi;

import android.app.AlertDialog;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class homeActivity extends ActionBarActivity {

    //ATRIBUTO DA CLASSE ALERTDIALOG
    private AlertDialog notice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    private void aboutNotice() {
        //CRIANDO O GERADOR DO ALERTDIALOG
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //TITULO DO AVISO
        //builder.setTitle("Go Taxi");

        //DEFININDO MENSAGEM EXIBIDA
        //builder.setMessage("Version - 0.1 - Beta");

        //CRIANDO O AVISO
        notice = builder.create();
        notice.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.actionEntrar) {
            return true;
        }
        if (id == R.id.actionCadastrar) {
            return true;
        }
        if (id == R.id.actionSobre) {
            aboutNotice();
        }
        return super.onOptionsItemSelected(item);
    }
}
